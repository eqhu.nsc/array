var a = 15;
var b = 4;
var c = 0;


// menggunakan operator penjumlahan
var d = a + b;
console.log(d);

// pengurangan
c = a - b;
console.log(`${a} - ${b} = ${c}`);

// Perkalian
c = a * b;
console.log(`${a} * ${b} = ${c}`);

// pemangkatan
c = a ** b;
console.log(`${a} ** ${b} = ${c}`);

// Pembagian
c = a / b;
console.log(`${a} / ${b} = ${c}`);

// Modulo
c = a % b;
console.log(`${a} % ${b} = ${c}`);


//operator penugasan


console.log("Mula-mula nilai score...");
// pengisian nilai
var score = 100;
console.log("score = "+ score );

// pengisian dan menjumlahan dengan 5
score += 5;
console.log("score = "+ score);

// pengisian dan pengurangan dengan 2
score -= 2;
console.log("score = "+ score);

// pengisian dan perkalian dengan 2
score *= 2;
console.log("score = "+ score);

// pengisian dan pembagian dengan 4
score /= 4;
console.log("score = "+ score);

// pengisian dan pemangkatan dengan 2
score **= 2;
console.log("score = "+ score);

// pengisian dan modulo dengan 3;
score %= 3;
console.log("score = "+ score);